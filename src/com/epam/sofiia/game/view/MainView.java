package com.epam.sofiia.game.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Represents main view of the application
 *
 * @author Sofiia Reminska
 */
public class MainView extends Application {

    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;

    /**
     * Method for starting main window of application.
     *
     * @param primaryStage - object of type Stage
     * @throws IOException - if fxml file can not be loaded
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("view.fxml"));
        primaryStage.setTitle("Tic Tac Toe");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
    }

    /**
     * Entry point for the application.
     *
     * @param args - input params for program
     */
    public static void main(String[] args) {
        launch(args);
    }
}
