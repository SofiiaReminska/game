package com.epam.sofiia.game.controller;

import com.epam.sofiia.game.model.GameModel;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Represents actions with the game.
 */
public class Controller {
    /**
     * CIRCLE - variable responsible for the circle action of the game.
     */
    private static final String CIRCLE = "\u25EF";
    /**
     * CROSS - variable responsible for the cross action of the game.
     */
    private static final String CROSS = "\u2A09";
    /**
     * FONT_SIZE - font size for the label.
     */
    private static final int FONT_SIZE = 70;
    /**
     * stateOfGame - is responsible for the move in the game: the cross - true, or the zero - false.
     */
    private boolean stateOfGame = true;
    /**
     * model - object of type GameModel.
     */
    private GameModel model = new GameModel();

    @FXML
    public GridPane gridPane;
    @FXML
    public Pane mainPane;

    /**
     * Method for playing the game by click on the GridPane.
     *
     * @param mouseEvent - object of type MouseEvent
     */
    public void clickOnGame(MouseEvent mouseEvent) {
        final Label labelO = new Label(CIRCLE);
        final Label labelX = new Label(CROSS);
        final Font font = Font.font(FONT_SIZE);
        labelO.setFont(font);
        labelX.setFont(font);
        Node clickedNode = mouseEvent.getPickResult().getIntersectedNode();
        Integer colIndex = GridPane.getColumnIndex(clickedNode);
        Integer rowIndex = GridPane.getRowIndex(clickedNode);
        final GridPane source = (GridPane) mouseEvent.getSource();

        if (stateOfGame) {
            source.add(labelX, colIndex, rowIndex);
            model.game[colIndex][rowIndex] = CROSS;
            stateOfGame = false;
        } else {
            source.add(labelO, colIndex, rowIndex);
            model.game[colIndex][rowIndex] = CIRCLE;
            stateOfGame = true;
        }
        if (model.hasWon(model.game, colIndex, rowIndex)) {
            if (!stateOfGame) {
                showAlertMessage("X won!");
            } else {
                showAlertMessage("O won!");
            }
        } else if (model.isDraw()) {
            showAlertMessage("Draw!");
        }
    }

    /**
     * Method for showing an alert message about the game.
     *
     * @param msg - message which is need to be shown
     */
    private void showAlertMessage(String msg) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Tic tac toe");
        alert.setHeaderText("Result:");
        alert.setContentText(msg);
        ButtonType buttonTypeOne = new ButtonType("Try again");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            model.initGame();
            final List<Node> nodes = gridPane.getChildren().stream()
                    .filter(child -> child instanceof Label).collect(Collectors.toList());
            gridPane.getChildren().removeAll(nodes);
        } else {
            System.exit(0);
        }
    }
}
