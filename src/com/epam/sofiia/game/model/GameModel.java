package com.epam.sofiia.game.model;

/**
 * Represents model of the game.
 */
public class GameModel {
    /**
     * public constructor of the class GameModel.
     */
    public GameModel() {
        initGame();
    }

    /**
     * GAME_SIZE - size of game mode;
     */
    private static final int GAME_SIZE = 3;
    /**
     * game - 2d array of the game, stores crosses and circles.
     */
    public String[][] game = new String[GAME_SIZE][GAME_SIZE];

    /**
     * Method for determining the winner of the game.
     *
     * @param game     - 2d array of the game, stores crosses and circles.
     * @param colIndex - column position of current sign
     * @param rowIndex - row position of current sign
     * @return - true - game is won, false - game continues or draw
     */
    public boolean hasWon(String[][] game, Integer colIndex, Integer rowIndex) {
        String currentSymbol = game[colIndex][rowIndex];
        return (game[colIndex][0].equals(currentSymbol)
                && game[colIndex][1].equals(currentSymbol)
                && game[colIndex][2].equals(currentSymbol)
                || game[0][rowIndex].equals(currentSymbol)
                && game[1][rowIndex].equals(currentSymbol)
                && game[2][rowIndex].equals(currentSymbol)
                || rowIndex.equals(colIndex)
                && game[0][0].equals(currentSymbol)
                && game[1][1].equals(currentSymbol)
                && game[2][2].equals(currentSymbol)
                || rowIndex + colIndex == 2
                && game[0][2].equals(currentSymbol)
                && game[1][1].equals(currentSymbol)
                && game[2][0].equals(currentSymbol));
    }

    /**
     * Method for checking whether game should end or continue.
     *
     * @return - true - end game, false - continue game
     */
    public boolean isDraw() {
        for (int row = 0; row < 3; ++row) {
            for (int col = 0; col < 3; ++col) {
                if (game[row][col].equals("")) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Method initializing the 2d array of the game.
     */
    public void initGame() {
        for (int i = 0; i < game.length; i++) {
            for (int j = 0; j < game[i].length; j++) {
                game[i][j] = "";
            }
        }
    }
}
